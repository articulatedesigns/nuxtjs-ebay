export default async function({$auth, redirect, store}) {
    let user = $auth.$state.user;
    if(user && user.confirmed) {
        // let the user in
    } else {
        // dispatch error message
        store.dispatch("snackbar/setSnackbar", {
          color: "error",
          text: `You must be authorized to view that page.`
        });
        // redirect
        redirect('/login')
    }
}