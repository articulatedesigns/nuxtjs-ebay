export const state = () => ({
  all: []
});

export const actions = {
  async fetchAllSites({ commit }) {
    const sites = await this.$axios.$get(
      `/sites?_sort=title&users.username=finedesignz`
    );
    console.log(sites);
    commit("setSites", sites);
  }
};

export const mutations = {
  setSite(state, site) {
    state.all.push(site);
  },
  setSites(state, sites) {
    state.all = sites;
  }
};
